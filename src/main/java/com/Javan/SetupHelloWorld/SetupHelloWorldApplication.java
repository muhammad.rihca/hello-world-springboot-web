package com.Javan.SetupHelloWorld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SetupHelloWorldApplication {

	public static void main(String[] args) {
		SpringApplication.run(SetupHelloWorldApplication.class, args);
	}

}
