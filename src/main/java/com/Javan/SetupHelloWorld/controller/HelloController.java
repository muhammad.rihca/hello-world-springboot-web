package com.Javan.SetupHelloWorld.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class HelloController {

    @GetMapping("/hello")
    public String hello(Model model) {
        String message = "Hello Wordl";
        model.addAttribute("msg",message);
        return "index";
    }
}
